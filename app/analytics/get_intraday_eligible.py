import quandl
import pandas as pd
from datetime import date
from datetime import timedelta
from config import load_api_key
from metadata_util import get_all_index_codes

load_api_key()


def get_last_one_year_average(index, metadata):
    codes = get_all_index_codes(index, metadata)
    print codes
    end_date = date.today()
    start_date = end_date - timedelta(365)
    result = quandl.get(codes, end_date=str(end_date), start_date=str(start_date))
    print result
