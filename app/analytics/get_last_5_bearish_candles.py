import quandl
import pandas as pd
import numpy as np
import urllib
from config import load_api_key


def isRed(data, stockName):
    openColumnName = "Open"
    closeColumnName = "Close"
    if stockName != None:
        openColumnName = "{} - {}".format(stockName, openColumnName)
        closeColumnName = "{} - {}".format(stockName, closeColumnName)

    return data[openColumnName] > data[closeColumnName]


def isRedWrtPrevious(data, dataPrevious, stockName):
    openColumnName = "Open"
    closeColumnName = "Close"
    if stockName != None:
        openColumnName = "{} - {}".format(stockName, openColumnName)
        closeColumnName = "{} - {}".format(stockName, closeColumnName)

    return data[openColumnName] > data[closeColumnName] and dataPrevious[openColumnName] > data[openColumnName]


def getLastNContigiousBearish(stockName, rows=5):
    load_api_key()
    data = quandl.get(stockName, rows=rows, returns="numpy")
    # print data
    if isRed(data[0], stockName):
        allRed = True
        for i in range(1, len(data) - 1):
            if not isRed(data[i], data[i - 1], None):
                allRed = False
                break
        return allRed, data
    else:
        return False, data


def getLastNMinusOneContigiousBearishAndLastBullish(stockName, rowsToQuery=6):
    result = False
    load_api_key()
    try:
        data = quandl.get(stockName, rows=rowsToQuery, returns="numpy")
    except RuntimeError, e:
        print "Qundl error for code {}".format(stockName), e
        raise e

    openColumnName = "Open"
    closeColumnName = "Close"
    # print data
    allRed = True
    if isRed(data[0], None):
        for i in range(1, rowsToQuery - 1):
            # print "stockName={} comparing {} and {}".format(stockName,i,i-1)
            if not isRedWrtPrevious(data[i], data[i - 1], None):
                allRed = False
                break
        lastElementIndex = len(data) - 1
        if allRed and (data[lastElementIndex][openColumnName] < data[lastElementIndex][closeColumnName]):
            result = True

        return result, data
    else:
        return False, data


def getBearishCandles(metadata, numberOfDays=5):
    data = pd.read_csv(metadata)
    candles = []
    for index, row in data.iterrows():
        # print row
        code = row['code']
        allRed, candle = getLastNContigiousBearish("NSE/{}".format(urllib.quote(code, safe='')), numberOfDays)
        if allRed:
            candles.append(candle)
            print "Last five candles for {} are bearish.Last date={}".format(code, candle[numberOfDays - 1])
    return candles


def getLastNMinus1BearishAndLastBullishCandles(metadata, numberOfDays=6):
    data = pd.read_csv(metadata)
    candles = []
    for index, row in data.iterrows():
        # print row
        code = row['code']
        allRed, candle = getLastNMinusOneContigiousBearishAndLastBullish("NSE/{}".format(urllib.quote(code, safe='')),
                                                                         numberOfDays)
        if allRed:
            candles.append(candle)
            lastCandle = candle[numberOfDays - 1]
            secondLastCandleLow = candle[numberOfDays - 2]['Low']
            lastClose = lastCandle['Close']
            stopLoss = secondLastCandleLow - (0.002 * secondLastCandleLow)
            profitBooking = lastClose + 2 * (lastClose - stopLoss)

            print "Last five candles for {} are bearish and sixth is bullish. Purchase={} SL={} ProfitBooking={}.Last date={}".format(
                code, lastClose, stopLoss, profitBooking, candle[
                    numberOfDays - 1])
    return candles


def getBearishCandlesBulk(metadata, numberOfDays=5):
    data = pd.read_csv(metadata)
    codes = data['code'].values
    for i in range(len(data['code'].values)):
        codes[i] = "NSE/{}".format(urllib.quote(codes[i], safe=''))

    print codes
