import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from pykalman import KalmanFilter

print "Reding CSV"

data = pd.read_csv("BSE-BOM532528.csv")
# print data.head()
# print data.abs()
data['Date'] = pd.to_datetime(data['Date'], format="%Y-%m-%d")

df = pd.DataFrame(data)

kf = KalmanFilter(initial_state_mean=0, n_dim_obs=1)
means, covariances = kf.filter(df[["Open"]])
next_mean, next_covariance = kf.filter_update(
    means[-1], covariances[-1]
)
predictedDf = pd.DataFrame([['2018-08-30', next_mean]], columns=[['Date'], ['Open']])
df.append(predictedDf)

print "Next Mean :{0}".format(next_mean)
print means
print covariances
print df.columns[0]
fig, ax = plt.subplots()
openIdx = data[["Date", "Open"]]

print openIdx.head()
ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')
print openIdx.dtypes
print df['Date']
openIdx.plot(x='Date', y='Open', ax=ax)
plt.show()
