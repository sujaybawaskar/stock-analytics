import pandas as pd


def get_all_index_codes(index, metadata):
    data = pd.read_csv(metadata)
    codes = data['code'].values
    result = []
    for i in range(len(codes)):
        result.append("{}/{}".format(index, codes[i]))
    return result
